const { users } = require("../models");

module.exports = {
  create(createArgs) {
    return users.create(createArgs);
  },

  login(email){
    return users.findOne({
      where: { email },
    })
  },

  update(id, updateArgs) {
    return users.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return users.destroy(id);
  },

  find(id) {
    return users.findByPk(id);
  },

  findAll() {
    return users.findAll();
  },

  getTotalPost() {
    return users.count();
  },
};
