const { carlist } = require("../models");

module.exports = {
    create(createArgs) {
        return carlist.create(createArgs);
    },

    update(id, updateArgs) {
        return carlist.update(updateArgs, {
            where: {
                id,
            },
        });
    },

    delete(id) {
        return carlist.destroy(id);
    },

    find(id) {
        return carlist.findByPk(id);
    },

    findAll() {
        return carlist.findAll();
    },

    getTotalPost() {
        return carlist.count();
    },
};
