const carlistService = require("../../../services/carlistService");
const jwt = require("jsonwebtoken");

module.exports = {
    list(req, res) {
        carlistService
            .list()
            .then(({ data, count }) => {
                const newdata = data.filter(
                    (row) => row.status_delete == "false"
                );
                res.status(200).json({
                    status: "OK",
                    data: { posts: newdata },
                    meta: { total: count },
                });
            })
            .catch((err) => {
                res.status(400).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    create(req, res) {
        const bearerToken = req.headers.authorization;
        const token = bearerToken.split("Bearer ")[1];
        const tokenPayload = jwt.verify(
            token,
            process.env.JWT_SIGNATURE_KEY || "Rahasia"
        );

        if (tokenPayload.role == "member") {
            res.status(201).json({ message: "Tidak punya hak beb" });
            return;
        }

        const add_user = tokenPayload.email;

        carlistService
            .create({ ...req.body, add_user, status_delete: "false" })
            .then((post) => {
                res.status(201).json({
                    status: "OK",
                    data: post,
                });
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    update(req, res) {
        const bearerToken = req.headers.authorization;
        const token = bearerToken.split("Bearer ")[1];
        const tokenPayload = jwt.verify(
            token,
            process.env.JWT_SIGNATURE_KEY || "Rahasia"
        );

        if (tokenPayload.role == "member") {
            res.status(201).json({ message: "Tidak punya hak beb" });
            return;
        }

        const update_user = tokenPayload.name;

        carlistService
            .update(req.params.id, { ...req.body, update_user })
            .then(() => {
                res.status(200).json({
                    status: "OK",
                });
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    show(req, res) {
        carlistService
            .get(req.params.id)
            .then((post) => {
                if (post.status_delete == "true") {
                    res.status(201).json({ message: "Data kehapus beb" });
                    return;
                }
                res.status(200).json({
                    status: "OK",
                    data: post,
                });
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    history(req, res) {
        carlistService
            .list()
            .then(({ data, count }) => {
                const newdata = data.filter(
                    (row) => row.status_delete == "true"
                );
                res.status(200).json({
                    status: "OK",
                    data: { posts: newdata },
                    meta: { total: count },
                });
            })
            .catch((err) => {
                res.status(400).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    destroy(req, res) {
        const bearerToken = req.headers.authorization;
        const token = bearerToken.split("Bearer ")[1];
        const tokenPayload = jwt.verify(
            token,
            process.env.JWT_SIGNATURE_KEY || "Rahasia"
        );

        if (tokenPayload.role == "member") {
            res.status(201).json({ message: "Tidak punya hak beb" });
            return;
        }

        const delete_user = tokenPayload.email;

        carlistService
            .update(req.params.id, {
                ...req.body,
                delete_user,
                status_delete: "true",
            })
            .then(() => {
                res.status(200).json({
                    status: "OK",
                });
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },
};
