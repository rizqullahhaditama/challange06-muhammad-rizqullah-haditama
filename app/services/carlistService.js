const carlistRepo = require("../repositories/carlistRepo");

module.exports = {
  create(requestBody) {
    return carlistRepo.create(requestBody);
  },

  update(id, requestBody) {
    return carlistRepo.update(id, requestBody);
  },

  delete(id) {
    return carlistRepo.delete(id);
  },

  async list() {
    try {
      const posts = await carlistRepo.findAll();
      const postCount = await carlistRepo.getTotalPost();

      return {
        data: posts,
        count: postCount,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return carlistRepo.find(id);
  },
};
